job "demo-[[.environment]]" {
  datacenters = ["dc1"]

  group "demo-[[.environment]]" {
    count = 1

    ###########################
    task "server-[[.environment]]" {
      driver = "docker"

      env {
        PORT    = "${NOMAD_PORT_http}"
        NODE_IP = "${NOMAD_IP_http}"
      }

      config {
        image = "registry.gitlab.com/insignficant/learning-nomad:v1"
        #network_mode = "host"
      }

      service {
        name = "demo-[[.environment]]"
        port = "http"

        tags = [
          "traefik.enable=true",
          "traefik.tags=service",
          "traefik.frontend.rule=Host:[[.deploy_url]]"
        ]

        check {
          type     = "http"
          path     = "/"
          interval = "2s"
          timeout  = "2s"
        }
      }
    }
    ###########################

    network {
        port  "http"{}
    }

  }
}
